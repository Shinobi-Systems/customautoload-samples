module.exports = (s,config,lang,app,io) => {
    if(config.webBlocksPreloaded.indexOf('home/powerVideo') === -1){
        // config.webBlocksPreloaded.push('home/powerVideo')
        const newMenuItem = {
            icon: 'barcode',
            label: `${lang['Power Viewer']}`,
            pageOpen: 'powerVideo',
        }
        const sideMenuContents = s.definitions.SideMenu.blocks.Container1.links;
        const fileBinLinkIndex = sideMenuContents.findIndex(x => x.pageOpen === 'fileBinView');
        sideMenuContents.splice(fileBinLinkIndex + 1, 0, newMenuItem)
    }
}
