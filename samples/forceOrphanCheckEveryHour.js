module.exports = function(s,config,lang,app,io){
    const {
        scanForOrphanedVideos
    } = require('../video/utils.js')(s,config,lang)
    async function checkAllLoadedMonitors(){
        if(s.group){
            console.log(`Checking Orphans`)
            for (const groupKey in s.group) {
                const group = s.group[groupKey]
                for (const monitorId in group.rawMonitorConfigurations) {
                    const monitorConfig = group.rawMonitorConfigurations[monitorId]
                    const { orphanedFilesCount } = await scanForOrphanedVideos(monitorConfig,{forceCheck: true, checkMax: 4})
                    console.log(`Orphan Check ${groupKey} : ${monitorConfig.name}, found : ${orphanedFilesCount}`)
                }
            }
        }
    }
    checkAllLoadedMonitors()
    var everyHour = 1000 * 60 * 60
    setInterval(checkAllLoadedMonitors,everyHour)
}
