module.exports = function(s,config,lang,app,io){
    const {
        triggerEvent,
    } = require('../events/utils.js')(s,config,lang)
    const onvifEvents = require("node-onvif-events");
    const onvifEventIds = []
    const onvifEventControllers = {}
    const startMotion = async (onvifId,monitorConfig) => {
        const groupKey = monitorConfig.ke
        const monitorId = monitorConfig.mid
        const onvifIdKey = `${monitorConfig.mid}${monitorConfig.ke}`
        const controlBaseUrl = monitorConfig.details.control_base_url || s.buildMonitorUrl(monitorConfig, true)
        const controlURLOptions = s.cameraControlOptionsFromUrl(controlBaseUrl,monitorConfig)
        const onvifPort = parseInt(monitorConfig.details.onvif_port) || 8000
        let options = {
          id: onvifId,
          hostname: controlURLOptions.host,
          username: controlURLOptions.username,
          password: controlURLOptions.password,
          port: onvifPort,
        };
      const detector = onvifEventControllers[onvifIdKey] || await onvifEvents.MotionDetector.create(options.id, options);
      function onvifEventLog(type,data){
          s.userLog({
              ke: groupKey,
              mid: monitorId
          },{
              type: type,
              msg: data
          })
      }
      onvifEventLog(`ONVIF Event Detection Listening!`)
      try {
        detector.listen((motion) => {
          if (motion) {
            // onvifEventLog(`ONVIF Event Detected!`)
            triggerEvent({
                f: 'trigger',
                id: monitorId,
                ke: groupKey,
                details:{
                    plug: 'onvifEvent',
                    name: 'onvifEvent',
                    reason: 'motion',
                    confidence: 100,
                    // reason: 'object',
                    // matrices: [matrix],
                    // imgHeight: img.height,
                    // imgWidth: img.width,
                }
            })
          // } else {
              // onvifEventLog(`ONVIF Event Stopped`)
          }
        });
      } catch(e) {
          onvifEventLog(`ONVIF Event Error`,e)
      }
      return detector
    }
    function initializeOnvifEvents(monitorConfig){
        const monitorMode = monitorConfig.mode
        const groupKey = monitorConfig.ke
        const monitorId = monitorConfig.mid
        const hasOnvifEnabled = monitorConfig.details.is_onvif === '1'
        if(hasOnvifEnabled){
            const onvifIdKey = `${monitorConfig.mid}${monitorConfig.ke}`
            let onvifId = onvifEventIds.indexOf(onvifIdKey)
            if(onvifEventIds.indexOf(onvifIdKey) === -1){
                onvifId = onvifEventIds.length;
                onvifEventIds.push(onvifIdKey);
            }
            try{
                onvifEventControllers[onvifIdKey].close()
                delete(onvifEventControllers[onvifIdKey])
                s.debugLog('ONVIF Event Module Warning : This could cause a memory leak?')
            }catch(err){
                console.log('ONVIF Event Module Error', err);
            }
            if(monitorMode !== 'stop'){
                startMotion(onvifId,monitorConfig).then((detector) => {
                    onvifEventControllers[onvifIdKey] = detector;
                })
            }
        }
    }
    s.onMonitorStart((monitorConfig) => {
        initializeOnvifEvents(monitorConfig)
    })
}
