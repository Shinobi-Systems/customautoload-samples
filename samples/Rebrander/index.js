module.exports = (s,config,lang,app,io) => {
    config.poweredByShinobi = "Powered by Shinobi Systems"
    config.logoLocation76x76Link = "https://shinobi.systems"
    config.defaultTheme = "Ice-v3"
    // // make icon with https://favicomatic.com/ (need square) >
    // config.webFavicon = "libs/img/brand/favicon.ico"
    // config.logoLocationAppleTouchIcon = 'libs/img/brand/apple-touch-icon.png';
    // config.logoLocation57x57 = 'libs/img/brand/apple-touch-icon-57x57.png';
    // config.logoLocation72x72 = 'libs/img/brand/apple-touch-icon-72x72.png';
    // config.logoLocation76x76 = 'libs/img/brand/apple-touch-icon-76x76.png';
    // config.logoLocation114x114 = 'libs/img/brand/apple-touch-icon-114x114.png';
    // config.logoLocation120x120 = 'libs/img/brand/apple-touch-icon-120x120.png';
    // config.logoLocation144x144 = 'libs/img/brand/apple-touch-icon-144x144.png';
    // config.logoLocation152x152 = 'libs/img/brand/apple-touch-icon-152x152.png';
    // config.logoLocation196x196 = 'libs/img/brand/favicon-196x196.png';
    // // make icon with https://favicomatic.com  />
    // config.showLoginSelector = false;
    // config.showLoginTypeSelector = false;
    // config.logoLocation76x76Style = ""
    // config.loginScreenBackground = "libs/img/brand/background.jpg"
    // config.loginScreenBackgroundCss = "background-size: contain;opacity: 0.2;background-position-y: center;"
    config.socialLinks = [
        {
            icon: 'home',
            href: 'https://shinobi.video',
            title: 'Homepage'
        },
        {
            icon: 'facebook',
            href: 'https://www.facebook.com/ShinobiCCTV',
            title: 'Facebook'
        },
        {
            icon: 'twitter',
            href: 'https://twitter.com/ShinobiCCTV',
            title: 'Twitter'
        },
        {
            icon: 'youtube',
            href: 'https://www.youtube.com/channel/UCbgbBLTK-koTyjOmOxA9msQ',
            title: 'YouTube'
        }
    ];

    //// ejs file load.
    //// all ejs files in your customAutoLoad pages blocks folder will load automatically.
    // config.webBlocksPreloaded = [
    //     'home/initial',
    //     'home/videoPlayer',
    //     'home/monitorsList',
    //     'home/subAccountManager',
    //     'home/accountSettings',
    //     'home/apiKeys',
    //     'home/monitorSettings',
    //     'home/schedules',
    //     'home/monitorStates',
    //     'home/liveGrid',
    //     'home/regionEditor',
    //     // 'home/timelapseViewer',
    //     'home/eventFilters',
    //     'home/cameraProbe',
    //     'home/powerVideo',
    //     'home/onvifScanner',
    //     'home/onvifDeviceManager',
    //     'home/configFinder',
    //     'home/logViewer',
    //     // 'home/calendar',
    //     'home/eventListWithPics',
    //     'home/fileBin',
    //     'home/videosTable',
    //     'home/studio',
    //     'home/monitorMap',
    //     'home/reportManager',
    //     'confirm',
    //     'home/help',
    // ];
    //// s.definitions.Theme.isDark = false;
    //// s.definitions['Monitor Options'].dropdownClass = '';
    s.definitions.SideMenu.blocks.Container1.links.push({
        icon: 'star',
        label: `Custom Tab`,
        pageOpen: 'customTab',
    });
    s.definitions["Custom Tab"] = {
        "section": "Custom Tab",
        "blocks": {
            "Search Settings": {
                "id": "cameraWizardSearchForm",
               "name": lang["Search"],
               "color": "navy",
               "section-pre-class": "col-md-4",
               "info": [
                   {
                      "name": "user",
                      "field": lang['Camera Username'],
                      "placeholder": "Can be left blank.",
                   },
                   {
                      "name": "pass",
                      "field": lang['Camera Password'],
                      "fieldType": "password",
                   },
                   {
                      "name": "ip",
                      "field": lang['IP Address'],
                      "description": lang['Range or Single'],
                      "example": "10.1.100.1-10.1.100.254",
                   },
                   {
                      "fieldType": "btn-group",
                      "btns": [
                          {
                              "fieldType": "btn",
                              "forForm": true,
                              "class": `btn-block btn-success`,
                              "btnContent": `${lang['Search']}<span class="_loading" style="display:none"> &nbsp; <i class="fa fa-pulse fa-spinner"></i></span>`,
                          },
                          {
                              "fieldType": "btn",
                              "class": `btn-default add-all`,
                              "btnContent": `${lang['Add All']}`,
                          },
                      ],
                   },
              ]
          },
          "Found Devices": {
             "name": lang['Found Devices'],
             "color": "blue",
             "section-pre-class": "col-md-8",
             "info": [
                 {
                     "fieldType": "div",
                     "class": "onvif_result row",
                 }
             ]
         },
          "Other Devices": {
             "name": lang['Other Devices'],
             "color": "danger",
             "section-pre-class": "col-md-12",
             "info": [
                 {
                     "fieldType": "div",
                     "class": "onvif_result_error row",
                 }
             ]
         },
       }
    }


     // delete(s.definitions["Account Settings"].blocks.ShinobiHub);
     // Object.keys(s.definitions["Account Settings"].blocks).forEach((keyName) => {
     //     if(keyName !== 'Monitor Groups'){
     //         const aBlock = s.definitions["Account Settings"].blocks[keyName];
     //         aBlock['section-class'] = aBlock['section-class'] ? (aBlock['section-class'] + ` hide-box-wrapper`) : 'hide-box-wrapper';
     //     }
     // })
     // Object.keys(s.definitions["Monitor Settings"].blocks).forEach((keyName) => {
     //     const aBlock = s.definitions["Monitor Settings"].blocks[keyName];
     //     aBlock['section-class'] = aBlock['section-class'] ? (aBlock['section-class'] + ` hide-box-wrapper`) : 'hide-box-wrapper';
     // })
     // //remove theme selection field from account settings >
     // const accountSettingsPrefsInfo = s.definitions["Account Settings"].blocks.Preferences.info;
     // const themeFieldIndex = accountSettingsPrefsInfo.findIndex(x => x.name ==="detail=theme");
     // accountSettingsPrefsInfo.splice(themeFieldIndex, 1)
     // //remove theme selection field from account settings />
     config.staticUsers = [
         {
             mail: 'shinobi@shinobi.video',
             initialPassword: 'shinobi',
         }
     ]
     // disable superuser access
     config.superUserLoginDisabled = false;
}
