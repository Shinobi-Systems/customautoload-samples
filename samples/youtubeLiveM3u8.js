module.exports = (s,config,lang,app,io) => {
    const fetch = require('node-fetch')
    const youtubedl = require('youtube-dl-exec')
    const fetchTimeout = (url, ms, { signal, ...options } = {}) => {
        const controller = new AbortController();
        const promise = fetch(url, { signal: controller.signal, ...options });
        if (signal) signal.addEventListener("abort", () => controller.abort());
        const timeout = setTimeout(() => controller.abort(), ms);
        return promise.finally(() => clearTimeout(timeout));
    }
    function getM3u8Data(ytId){
        return new Promise((resolve,reject) => {
            function onFail(err){
                console.log('YT DL ERROR')
                console.logs(err)
                resolve('')
            }
            youtubedl('https://www.youtube.com/watch?v=' + ytId, {
              dumpSingleJson: true,
              noCheckCertificates: true,
              noWarnings: true,
              preferFreeFormats: true,
              addHeader: [
                'referer:youtube.com',
                'user-agent:googlebot'
              ]
            }).then(output => {
                fetchTimeout(output.url,30000,{
                  method: 'GET',
                })
                .then(response => response.text())
                .then(output => {
                    resolve(output);
                }).catch(onFail)
            }).catch(onFail)
        })
    }
    app.get('/youtubeM3u8/:ytId', function (req,res){
        const ytId = req.params.ytId;
        getM3u8Data(ytId).then(response => res.end(response))
    });
}
