var fs = require('fs');
var Slimbot = require('slimbot');
module.exports = function(s,config,lang,app,io){
    var telegramToken = config.telegramToken || 'TELEGRAM_BOT_TOKEN'
    var telegramChannel = config.telegramChannel || 'TELEGRAM_CHANNEL_ID'
    var notifierBot = new Slimbot(telegramToken)
    var yourCustomEvent = function(d,filter){
        // notifierBot.sendMessage(telegramChannel, 'Message received')
        var fileUpload = fs.createReadStream(s.dir.streams+'/'+d.ke+'/'+d.id+'/s.jpg')
        notifierBot.sendPhoto(telegramChannel, fileUpload).then(message => {
            console.log(message.result.photo)
        })
    }
    s.onEventTrigger(yourCustomEvent)
    console.log('Loaded "customAutoLoad" Sample : On Event Trigger Send JPEG API Snapshot to Telegram')
}
