var execSync = require('child_process').execSync;
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
module.exports = function(s,config,lang,app,io){
    // console.log(config)
    s.newGlobalShinobiFunction = function(){}
    /**
    * API : Custom Script Run from API
    */
    app.get(config.webPaths.apiPrefix+':auth/customScript/:ke', function(req,res){
        res.header("Access-Control-Allow-Origin",req.headers.origin);
        s.auth(req.params,function(user){
            var endData = {
                ok: true
            }
            //run some stuff
            exec('ls /home/Shinobi',function(err,data){
                console.log(err,data)
                if(err){
                    endData.ok = false
                    endData.err = err
                }
                endData.data = data
                res.end(s.prettyPrint(endData))
            })
        },res,req);
    });
}
